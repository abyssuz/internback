const sequelize = require('../db');
const { DataTypes } = require('sequelize');

const Client = sequelize.define(
  'client',
  {
    id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
    name: { type: DataTypes.STRING, allowNull: false },
    email: { type: DataTypes.STRING, allowNull: false, unique: true },
  },
  { timestamps: false }
);

const City = sequelize.define(
  'city',
  {
    id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
    name: { type: DataTypes.STRING, allowNull: false },
  },
  { timestamps: false }
);

const Master = sequelize.define(
  'master',
  {
    id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
    name: { type: DataTypes.STRING, allowNull: false },
    rating: { type: DataTypes.ARRAY(DataTypes.INTEGER), defaultValue: [] },
  },
  { timestamps: false }
);

const Order = sequelize.define(
  'order',
  {
    id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
    clockSize: { type: DataTypes.STRING, allowNull: false },
    city: { type: DataTypes.STRING, allowNull: false },
    date: { type: DataTypes.STRING, allowNull: false },
    startTime: { type: DataTypes.INTEGER, allowNull: false },
    endTime: { type: DataTypes.INTEGER, allowNull: false },
    rateLink: { type: DataTypes.STRING },
  },
  { timestamps: false }
);

const MasterCity = sequelize.define(
  'master_city',
  {
    id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
  },
  { timestamps: false }
);

Client.hasMany(Order);
Order.belongsTo(Client);

Master.hasMany(Order);
Order.belongsTo(Master);

Master.belongsToMany(City, { through: MasterCity });
City.belongsToMany(Master, { through: MasterCity });
Master.hasMany(MasterCity);
City.hasMany(MasterCity);
MasterCity.belongsTo(Master);
MasterCity.belongsTo(City);

module.exports = {
  Client,
  City,
  Master,
  Order,
  MasterCity,
};
