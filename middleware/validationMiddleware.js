const { Op } = require('sequelize');
const getNeededTime = require('../controllers/assets/getNeededTime');
const ApiError = require('../error/ApiError');
const { City, Master, Order } = require('../models/models');

module.exports = async (req, res, next) => {
  if (req.method === 'OPTIONS') {
    next();
  }

  const endTime = getNeededTime(req.body.clockSize);

  try {
    const city = await City.findOne({ where: { id: req.body.cityId } });
    const orders = await Order.findAll({
      where: {
        city: city.name,
        date: req.body.date,
        masterId: req.body.masterId,
        [Op.or]: {
          startTime: { [Op.and]: [{[Op.gte]: +req.body.time}, {[Op.lt]: +req.body.time + endTime}] },
          endTime: { [Op.and]: [{[Op.gt]: +req.body.time}, {[Op.lte]: +req.body.time + endTime}] },
        },
      },
    });

    if (orders.length > 0)
      return next(ApiError.badRequest('Cant create order with this options'));

    Object.keys(req.body).forEach(async (field) => {
      switch (field) {
        case 'name':
          if (typeof req.body[field] !== 'string' || req.body[field].length < 3)
            return next(ApiError.badRequest('invalid name'));
          break;
        case 'email':
          const emailRegex =
            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
          if (!emailRegex.test(req.body[field]))
            return next(ApiError.badRequest('invalid email'));
          break;
        case 'cityId':
          if (city === null) return next(ApiError.notFound('City undefined'));
          break;
        case 'masterId':
          const master = await Master.findOne({
            where: { id: req.body[field] },
          });
          if (master === null)
            return next(ApiError.notFound('Master undefined'));
          break;
        case 'clockSize':
          if (
            req.body[field] === 'small' ||
            req.body[field] === 'medium' ||
            req.body[field] === 'large'
          )
            break;
          return next(ApiError.badRequest('invalid clock size'));
        case 'date':
          const d = new Date();
          const [year, month, day] = req.body[field].split('-');
          if (
            +year < d.getFullYear() ||
            +month < d.getMonth() + 1 ||
            +day < 1 ||
            +day > 31
          )
            return next(ApiError.badRequest('invalid date'));
          break;
        case 'time':
          if (+req.body[field] < 9 || +req.body[field] > 17)
            return next(ApiError.badRequest('invalid time'));
          break;
      }
    });
    next();
  } catch (e) {
    return next(ApiError.internal('internal error'));
  }
};
