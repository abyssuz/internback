const nodemailer = require('nodemailer');

class Mailer {
  constructor() {
    this.transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        user: process.env.MAILER_USER,
        pass: process.env.MAILER_PASSWORD,
      },
    });
  }

  sendOrder({ name, email, clockSize, city, date, time, rateLink }) {
    const message = {
      from: process.env.MAILER_USER,
      to: email,
      subject: 'Clockware order info',
      text: '',
      html: `
        <h1>Your order details</h1>
        <div>
          <p>Name: ${name}</p>
          <p>Clocksize: ${clockSize}</p>
          <p>Date: ${date}</p>
          <p>City: ${city}</p>
          <p>Time: ${time}:00</p>
          <a href="${process.env.CLIENT_URL}/ratemaster?link=${rateLink}">You can also rate master</a>
        </div>
      `,
    };

    this.transporter.sendMail(message, (err, info) => {
      if (err) {
        console.log(err);
      } else {
        console.log(info);
      }
    });
  }
}

module.exports = new Mailer();
