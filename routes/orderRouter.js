const Router = require('express');
const OrderController = require('../controllers/OrderController');
const authMiddleware = require('../middleware/authMiddleware');
const validationMiddleware = require('../middleware/validationMiddleware');
const router = new Router();

router.post('/', validationMiddleware, OrderController.create);
router.delete('/:id', authMiddleware, OrderController.delete);
router.get('/', authMiddleware, OrderController.getAll);
router.get('/:link', OrderController.getForRate);

module.exports = router;
