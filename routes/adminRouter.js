const Router = require('express');
const AdminController = require('../controllers/AdminController');
const authMiddleware = require('../middleware/authMiddleware');
const router = new Router();

router.post('/', AdminController.login);
router.get('/', authMiddleware, AdminController.auth);

module.exports = router;
