const Router = require('express');
const router = new Router();
const clientRouter = require('./clientRouter')
const orderRouter = require('./orderRouter');
const cityRouter = require('./cityRouter');
const masterRouter = require('./masterRouter');
const adminRouter = require('./adminRouter');

router.use('/clients', clientRouter);
router.use('/orders', orderRouter);
router.use('/cities', cityRouter);
router.use('/masters', masterRouter);
router.use('/admin', adminRouter);

module.exports = router;
