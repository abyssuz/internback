const Router = require('express');
const ClientController = require('../controllers/ClientController');
const authMiddleware = require('../middleware/authMiddleware');
const router = new Router();

router.delete('/:id', authMiddleware, ClientController.delete);
router.get('/', authMiddleware, ClientController.getAll);

module.exports = router;
