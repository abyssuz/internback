const Router = require('express');
const MasterController = require('../controllers/MasterController');
const authMiddleware = require('../middleware/authMiddleware');
const router = new Router();

router.post('/', authMiddleware, MasterController.create);
router.put('/:id', authMiddleware, MasterController.edit);
router.delete('/:id', authMiddleware, MasterController.delete);
router.get('/', authMiddleware, MasterController.getAll);
router.put('/rate/:id', MasterController.rate);
router.get('/free', MasterController.getFree);

module.exports = router;
