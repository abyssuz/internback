const Router = require('express');
const CityController = require('../controllers/CityController');
const authMiddleware = require('../middleware/authMiddleware');
const router = new Router();

router.post('/', authMiddleware, CityController.create);
router.delete('/:id', authMiddleware, CityController.delete);
router.put('/:id', authMiddleware, CityController.edit);
router.get('/', CityController.getAll);

module.exports = router;
