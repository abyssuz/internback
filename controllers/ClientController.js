const { Client, Order } = require('../models/models');
const ApiError = require('../error/ApiError');
const validation = require('./assets/validations');

class ClientController {
  async delete(req, res, next) {
    const { id } = req.params;

    if (validation.id(id)) {
      try {
        await sequelize.transaction(async (t) => {
          const client = await Client.findOne({
            where: { id },
            transaction: t,
          });
          if (!client) throw new Error('undefined id');

          await Order.destroy({
            where: { clientId: client.id },
            transaction: t,
          });
          await Client.destroy({ where: { id: client.id }, transaction: t });
        });
        return res.status(200).json(id);
      } catch (e) {
        return next(ApiError.internal(e));
      }
    } else return next(ApiError.badRequest('Invalid id'));
  }

  async getAll(req, res) {
    const clients = await Client.findAll();
    return res.status(200).json(clients);
  }
}

module.exports = new ClientController();
