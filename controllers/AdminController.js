const jwt = require('jsonwebtoken');

const generateJwt = (email) => {
  return jwt.sign({ email, auth: true }, process.env.TOKEN_SECRET_KEY, {
    expiresIn: '24h',
  });
};

class ClientController {
  async login(req, res) {
    const { email, password } = req.body;

    if (
      email === process.env.ADMIN_EMAIL &&
      password === process.env.ADMIN_PASSWORD
    ) {
      const token = generateJwt(email);
      return res.status(200).json({ token });
    } else return res.status(401).json({ message: 'wrong email or password' });
  }

  async auth(req, res) {
    return res.status(200).json('');
  }
}

module.exports = new ClientController();
