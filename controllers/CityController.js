const sequelize = require('../db');
const { City, MasterCity } = require('../models/models');
const ApiError = require('../error/ApiError');
const validation = require('./assets/validations');

class CityController {
  async create(req, res, next) {
    const { name } = req.body;

    if (validation.name(name)) {
      try {
        const createdCity = await sequelize.transaction(async (t) => {
          const city = await City.create({ name }, { transaction: t });
          return city;
        });
        return res.status(201).json(createdCity);
      } catch (e) {
        return next(ApiError.internal(e));
      }
    } else return next(ApiError.badRequest('Invalid name'));
  }

  async edit(req, res, next) {
    const { id } = req.params;
    const { name } = req.body;

    if (validation.id(id) && validation.name(name)) {
      try {
        const [editedCity] = await sequelize.transaction(async (t) => {
          const city = await City.findOne({ where: { id }, transaction: t });
          if (!city) throw new Error('undefined id');

          const [_, edited] = await City.update(
            { name },
            {
              where: { id: city.id },
              transaction: t,
              returning: true,
            }
          );
          return edited;
        });
        return res.status(201).json(editedCity);
      } catch (e) {
        return next(ApiError.internal(e));
      }
    } else return next(ApiError.badRequest('Invalid inputs'));
  }

  async delete(req, res, next) {
    const { id } = req.params;

    if (validation.id(id)) {
      try {
        await sequelize.transaction(async (t) => {
          const city = await City.findOne({ where: { id }, transaction: t });
          if (!city) throw new Error('undefined id');

          await City.destroy({ where: { id: city.id }, transaction: t });
          await MasterCity.destroy({
            where: { cityId: city.id },
            transaction: t,
          });
        });
        return res.status(200).json(id);
      } catch (e) {
        return next(ApiError.internal(e));
      }
    } else return next(ApiError.badRequest('Invalid id'));
  }

  async getAll(req, res) {
    const cities = await City.findAll();
    return res.status(200).json(cities);
  }
}

module.exports = new CityController();
