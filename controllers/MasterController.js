const sequelize = require('../db');
const { Op } = require('sequelize');
const { Master, City, Order, MasterCity } = require('../models/models');
const ApiError = require('../error/ApiError');
const validation = require('./assets/validations');
const getNeededTime = require('./assets/getNeededTime');

class MasterController {
  async create(req, res, next) {
    const { citiesId, name } = req.body;
    const validCitiesId = citiesId.filter((id) => validation.id(id));

    if (validation.name(name) && validCitiesId.length === citiesId.length) {
      try {
        const createdMaster = await sequelize.transaction(async (t) => {
          const master = await Master.create({ name }, { transaction: t });
          const masterCities = await City.findAll({
            where: { id: { [Op.or]: citiesId } },
            transaction: t,
          });

          const bulkRecords = masterCities.map((el) => {
            return { cityId: el.id, masterId: master.id };
          });

          await MasterCity.bulkCreate(bulkRecords, { transaction: t });

          return master;
        });
        return res.status(201).json(createdMaster);
      } catch (e) {
        return next(ApiError.internal(e));
      }
    } else return next(ApiError.badRequest('Invalid inputs'));
  }

  async edit(req, res, next) {
    const { id } = req.params;
    const { citiesId, name } = req.body;

    const validCitiesId = citiesId.filter((id) => validation.id(id));

    if (
      validation.id(id) &&
      validation.name(name) &&
      validCitiesId.length === citiesId.length
    ) {
      try {
        const [editedMaster] = await sequelize.transaction(async (t) => {
          const master = await Master.findOne({
            where: { id },
            transaction: t,
          });
          if (!master) throw new Error('undefined id');

          const [_, edited] = await Master.update(
            { name },
            { where: { id: master.id }, transaction: t, returning: true }
          );

          citiesId.forEach(async (cityId) => {
            await MasterCity.findOrCreate({
              where: { cityId, masterId: master.id },
              defaults: { cityId, masterId: master.id },
              transaction: t,
            });
          });

          await MasterCity.destroy({
            where: { masterId: master.id, cityId: { [Op.notIn]: citiesId } },
            transaction: t,
          });
          return edited;
        });
        editedMaster.dataValues.citiesId = citiesId;
        return res.status(201).json(editedMaster);
      } catch (e) {
        return next(ApiError.internal(e));
      }
    } else return next(ApiError.badRequest('Invalid inputs'));
  }

  async rate(req, res, next) {
    const { id } = req.params;
    const { rate } = req.body;
    const { link } = req.query;

    if (validation.id(id) && validation.rate(rate)) {
      try {
        await sequelize.transaction(async (t) => {
          const master = await Master.findOne({
            where: { id },
            transaction: t,
          });
          if (!master) throw new Error('undefined id');

          await Master.update(
            { rating: [...master.rating, +rate] },
            { where: { id: master.id }, transaction: t }
          );

          await Order.update({ rateLink: '' }, { where: { rateLink: link } });
        });
        return res.status(201);
      } catch (e) {
        return next(ApiError.internal(e));
      }
    } else return next(ApiError.badRequest('Invalid inputs'));
  }

  async delete(req, res, next) {
    const { id } = req.params;
    if (validation.id(id)) {
      try {
        await sequelize.transaction(async (t) => {
          const master = await Master.findOne({
            where: { id },
            transaction: t,
          });
          if (!master) throw new Error('undefined id');

          await Master.destroy({ where: { id: master.id }, transaction: t });
          await MasterCity.destroy({
            where: { masterId: master.id },
            transaction: t,
          });
        });
        return res.status(200).json(id);
      } catch (e) {
        return next(ApiError.internal(e));
      }
    } else return next(ApiError.badRequest('Invalid inputs'));
  }

  async getFree(req, res, next) {
    const { clockSize, date, time, cityId } = req.query;

    if (
      validation.clockSize(clockSize) &&
      validation.date(date) &&
      validation.time(time) &&
      validation.id(cityId)
    ) {
      const neededTime = getNeededTime(clockSize);
      try {
        const orders = await Order.findAll({
          where: {
            date,
            startTime: { [Op.lt]: +time + neededTime },
            endTime: { [Op.gt]: +time },
          },
          attributes: { exclude: 'rateLink' },
        });

        const busyMastersId = orders.map(({ masterId }) => masterId);

        const cityMasters = await MasterCity.findAll({
          where: { cityId },
          include: {
            model: Master,
            where: { id: { [Op.notIn]: busyMastersId } },
          },
        });

        const freeMasters = cityMasters.map(({ master }) => master);

        return res.status(200).json(freeMasters);
      } catch (e) {
        return next(ApiError.internal(e));
      }
    } else return next(ApiError.badRequest('Invalid inputs'));
  }

  async getAll(req, res) {
    let masters = await Master.findAll({
      include: { model: MasterCity, attributes: ['cityId'] },
    });

    masters = masters.map(({ id, name, rating, master_cities }) => {
      return {
        id,
        name,
        rating,
        citiesId: master_cities.map((el) => el.cityId),
      };
    });

    return res.status(200).json(masters);
  }
}

module.exports = new MasterController();
