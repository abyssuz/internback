module.exports = {
  name: (target) => {
    if (typeof target === 'string' && target.length >= 3) return true;
    return false;
  },
  id: (target) => {
    if (!isNaN(target) && +target > 0) return true;
    return false;
  },
  rate: (target) => {
    if (!isNaN(target) && +target >= 1 && +target <= 5) return true;
    return false;
  },
  clockSize: (target) => {
    if (target === 'small' || target === 'medium' || target === 'large')
      return true;
    return false;
  },
  date: (target) => {
    const d = new Date();
    const [year, month, day] = target.split('-');
    if (
      +year >= d.getFullYear() &&
      +month >= d.getMonth() + 1 &&
      +day >= 1 &&
      +day <= 31
    )
      return true;
    return false;
  },
  time: (target) => {
    if (+target >= 9 && +target <= 17) return true;
    return false;
  },
};
