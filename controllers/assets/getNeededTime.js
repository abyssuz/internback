module.exports = (clocksize) => {
  switch (clocksize) {
    case 'small':
      return 1;
    case 'medium':
      return 2;
    case 'large':
      return 3;
  }
};
