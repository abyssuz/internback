const sequelize = require('../db');
const getNeededTime = require('./assets/getNeededTime');
const Mailer = require('../mailer/Mailer');
const uuid = require('uuid');
const { Client, Order, City, Master } = require('../models/models');
const ApiError = require('../error/ApiError');
const validation = require('./assets/validations');

class OrderController {
  async create(req, res, next) {
    const { name, email, clockSize, cityId, date, time, masterId } = req.body;
    const endTime = getNeededTime(clockSize) + +time;
    const rateLink = uuid.v4();

    try {
      const city = await City.findOne({ where: { id: cityId } });

      const createdOrder = await sequelize.transaction(async (t) => {
        const [client] = await Client.findOrCreate({
          where: { email },
          defaults: { name, email },
          transaction: t,
        });

        const order = await Order.create(
          {
            clockSize,
            city: city.name,
            date,
            startTime: time,
            endTime,
            clientId: client.id,
            masterId,
            rateLink,
          },
          { transaction: t }
        );
        return order;
      });

      Mailer.sendOrder({
        name,
        email,
        clockSize,
        city: city.name,
        date,
        time,
        rateLink,
      });

      return res.status(201).json(createdOrder);
    } catch (e) {
      return next(ApiError.internal(e.message));
    }
  }

  async delete(req, res, next) {
    const { id } = req.params;

    if (validation.id(id)) {
      try {
        await sequelize.transaction(async (t) => {
          const order = await Order.findOne({ where: { id }, transaction: t });
          if (!order) throw new Error('undefined id');

          await Order.destroy({ where: { id: order.id }, transaction: t });
        });
        return res.status(200).json(id);
      } catch (e) {
        return next(ApiError.internal(e.message));
      }
    } else return next(ApiError.badRequest('Invalid inputs'));
  }

  async getAll(req, res) {
    const orders = await Order.findAll({ include: [Client, Master] });
    return res.status(200).json(orders);
  }

  async getForRate(req, res, next) {
    const { link } = req.params;

    try {
      const order = await Order.findOne({ where: { rateLink: link } });

      if (!order) return next(ApiError.badRequest('undefined link'));

      const master = await Master.findOne({
        where: { id: order.masterId },
      });

      return res.status(200).json(master);
    } catch (e) {
      return next(ApiError.internal(e.message));
    }
  }
}

module.exports = new OrderController();
